/!\ THIS EXECUTABLE HAS BEEN TESTED UNDER WINDOWS 10 /!\

For the moment, we have to start the server by specifying manually the IP address
and the port number (in the future version, this will be set automatically):

command :

	./botnet_slave.exe -- IP PORT
