use std::env;

use async_std::io::ReadExt;
use async_std::net::TcpStream;
use async_std::sync::{Mutex, Arc};
use async_std::{task, io, net::TcpListener};
use async_std::stream::StreamExt;
use regex::Regex;
use uuid::{Uuid};

extern crate sysinfo;
//extern crate local_ip;

#[async_std::main]
async fn main() -> io::Result<()>{

    /*******************CONFIGURATION*******************/

    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        println!("Usage: {} <ip> <port>", args[0]);
        return Ok(());
    }

    let ip = &args[1].as_str();
    let socket = &args[2].as_str();

    let ip_regex = Regex::new(r"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$").unwrap();
    let port_regex = Regex::new(r"^\d{1,5}$").unwrap();

    if !ip_regex.is_match(ip) || !port_regex.is_match(socket) {
        println!("Invalid IP or port");
        return Ok(());
    }

    /**********************PROGRAM**********************/
    
    let lock_vec : Arc<Mutex<u8>> = Arc::new(Mutex::new(0));

    let connections: Vec<TcpStream> = vec![];
    let connections = Arc::new(Mutex::new(connections));
    
    let listener = TcpListener::bind(format!("{}:{}", ip, socket)).await?;
    let mut incoming = listener.incoming();

    while let Some(stream) = incoming.next().await {
        let stream = stream?;
        let connections = connections.clone();
        let mut write_permission = connections.lock().await;
        write_permission.push(stream.clone());
        drop(write_permission);
        let lock_vec_ = Arc::clone(&lock_vec);
        task::spawn(on_connection(stream, connections, lock_vec_));
    }

    Ok(())
}

async fn on_connection(mut stream: TcpStream, connections: Arc<Mutex<Vec<TcpStream>>>, lock_vec_ : Arc<Mutex<u8>>) -> io::Result<()>{
    let addr = stream.peer_addr()?;

    let mut buffer = [0u8; 1024];
    loop {
        let len = stream.read(&mut buffer).await?;
        if len > 0 {
            let message_received = String::from_utf8_lossy(&buffer[..len]);
            //println!("{}", &message_received);

            //The message
            let to_split : Vec<&str> = message_received.as_ref().split(":").collect();

            //the code number
            let to_match = *to_split.get(0).unwrap();

            //the data
            let data = *to_split.get(1).unwrap();

            let lock_vec = Arc::clone(&lock_vec_);

            match to_match{

                "00001" => {
                    println!("the master is connected !");
                },

                "00002" => {
                    let splitted = data.split("!").collect::<Vec<&str>>();
                    println!("ping {}:{}", splitted[0], splitted[1]);

                    /*
                    match surge_ping::ping(splitted[0].parse().unwrap()).await {
                        Ok((IcmpPacket::V4(packet), duration)) => {
                            println!(
                                "{} bytes from {}: icmp_seq={} ttl={:?} time={:.2?}",
                                packet.get_size(),
                                packet.get_source(),
                                packet.get_sequence(),
                                packet.get_ttl(),
                                duration
                            );
                        }
                        Ok(_) => unreachable!(),
                        Err(e) => println!("{:?}", e),
                    };*/

                },

                _ => println!("code number not recognized")
            }

        } else {
            println!("Disconnected: {}", stream.peer_addr()?);
            let mut connections_guard = connections.lock().await;
            
            let client_index = connections_guard.iter().position(|x| 
                (*x).peer_addr().unwrap() == stream.peer_addr().unwrap()).unwrap();
            connections_guard.remove(client_index);

            break
        }
    }

    Ok(())
    
}