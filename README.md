# How to Make a Test

## Step 1: Start the slave
1. Open a PowerShell terminal and run the following command to start the slave for testing purposes:

./botnet_slave.exe 127.0.0.1 8080

Note: In a real scenario, the IP should be the actual IP of the server and the same applies to the port number (usually 80).

## Step 2: Run the master and launch an attack
1. Run `botnet_master.exe`
2. Add the slave `127.0.0.1:8080`
3. Launch an attack on a random IP
4. In the slave terminal, you should see the mention "ping IP_TARGET:PORT_TARGET" followed by "Disconnected: IP_TARGET:PORT_TARGET". This means that the master has connected to the slave to instruct it to "ping this guy" and then disconnected.
